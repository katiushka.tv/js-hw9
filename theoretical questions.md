1. Щоб створити новий HTML тег на сторінці, можна використати метод document.createElement(tag) і
потім вставити його в потрібному місці за допомогою методу append (prepend, before, after) або
метод elem.insertAdjacentHTML(куди, html).
2. Перший параметр методу elem.insertAdjacentHTML(куди, html) показує, куди вставити елемент, 
відносно заданого elem. Може приймати значення "beforebegin" – вставити html перед elem, 
"afterbegin" – вставити html в elem, на початку, "beforeend" – вставити html в elem, в кінці,
"afterend" – вставити html після elem.
3. Для видалення елемента зі сторінки можна використати метод node.remove().