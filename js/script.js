"use strict"

function makeList(array,parent = document.body) {
    let list = document.createElement("ul");
    parent.append(list);
    array.forEach(el => {
        let listItem = document.createElement("li");
        listItem.innerHTML = el;
        list.append(listItem);
    });
    setTimeout(() => list.remove(), 3000);
}
makeList(["1", "2", "3", "sea", "user", 23]);
makeList(["hello", "world", "Kyiv", "Kharkiv", "Odesa", "Lviv"]);